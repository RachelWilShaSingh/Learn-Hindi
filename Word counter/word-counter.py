documents = [
  "documents/wikipedia_कंप्यूटर.txt",
  "documents/wikipedia_वीडियो खेल.txt",
  "documents/wikipedia_सॉफ्टवेयर.txt",
  "documents/wikipedia_चाचा चौधरी.txt",
  "documents/wikipedia_कॉमिक्स.txt",
]

wordCount = {}

for docPath in documents:
  docFile = open( docPath )
  contents = docFile.readlines()

  for line in contents:
    words = line.split( " " )
    
    for word in words:
      word = word.replace( "। ", "" )
      word = word.replace( "\"", "" )
      word = word.replace( ",", "" )
      word = word.replace( ".", "" )
      
      if ( word in wordCount ):
        wordCount[ word ] += 1
      else:
        wordCount[ word ] = 1
      

outFile = open( "Summary.csv", "w" )
outFile.write( "WORD,COUNT\n" )
for word in wordCount:
  outFile.write( word + "," + str( wordCount[word] ) + "\n" )
