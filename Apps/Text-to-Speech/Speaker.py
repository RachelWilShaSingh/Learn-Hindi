#!/usr/bin/python
# -*- coding: utf-8 -*-

# This Python file uses the following encoding: utf-8
# https://pypi.python.org/pypi/gTTS

from gtts import gTTS

#phrases = [ { "Phrase" : "नमस्ते, क्या हाल है", "Filename" : "Namaste" },
#            { "Phrase" : " 	आप से मिलकर ख़ुशी हुई", "Filename" : "Pleased to meet you" }
# ]

def DoPhrases():
    phrases = []

    print( "\n Reading wordlist.txt..." )

    with open( "wordlist2.txt", "r" ) as infile:
        for line in infile:
            stripped = line.rstrip('\n')
            if ( stripped is not "" ):
                phrases.append( stripped )
                print( "\t" + str( len( phrases ) ) + ": " + stripped )

    print( "\n Generating phrases..." )

    counter = 0
    for phrase in phrases:
        counter += 1
        print( "\t " + str( counter ) + "/" + str( len( phrases ) ) + ": " + phrase )
        tts = gTTS( text = phrase, lang = "hi", slow = True )
        tts.save( "output/" + phrase + ".mp3" )

def DoSingle( word ):
    tts = gTTS( text = word, lang = "hi", slow = True )
    tts.save( word + ".mp3" )

DoSingle( "भिन्डी" )






