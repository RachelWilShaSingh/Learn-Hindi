$( document ).ready( function()
{
    var lines = [
        [ 'क', 'ख', 'ग', 'घ', 'ङ' ],
        [ 'च', 'छ', 'ज', 'झ', 'ञ' ],
        [ 'ट', 'ठ', 'ड', 'ढ', 'ण' ],
        [ 'त', 'थ', 'द', 'ध', 'न' ],
        [ 'प', 'फ', 'ब', 'भ', 'म' ],
        [ 'य', 'र', 'ल', 'व' ],
        [ 'श', 'ष', 'स', 'ह' ],
        [ 'क्ष', 'त्र', 'ज्ञ' ]
    ];

    var l, c;
    var totalCorrect = 0, totalQuestions = 0;

    function ListContains( list, item )
    {
        for ( var i = 0; i < list.length; list++ )
        {
            if ( list[i] === item )
            {
                return true;
            }
        }
        
        return false;
    }

    function GetLines( correctLine )
    {
        var usedLines = []
        
        for ( var i = 0; i < 5; i++ )
        {
            var rl = Math.floor( Math.random() * lines.length );
            while ( ListContains( usedLines, rl ) )
            {
                rl = Math.floor( Math.random() * lines.length );
            }
            usedLines.push( rl );            
        }

        // Randomly replace one of these
        var correct = Math.floor( Math.random() * 5 );
        usedLines[ correct ] = correctLine;
        
        return usedLines
    }

    function NewQuestion()
    {
        $( "#next-question" ).hide();
        $( "#message" ).html( "What is the missing letter? " );
        $( "#message" ).css( "color", "#2b9a28" );
        $( ".letter-entry" ).css( "color", "#000000" );
        
        l = Math.floor( Math.random() * lines.length );
        c = Math.floor( Math.random() * lines[l].length );

        console.log( l, c );
        
        for ( var i = 0; i < lines[l].length; i++ )
        {
            if ( i == c )
            {
                $("#letter" + (i+1)).val( "" );
            }
            else
            {
                $("#letter" + (i+1)).val( lines[l][i] );
            }
        }

        var usedLines = GetLines( l );

        for ( var i = 0; i < 5; i++ )
        {
            console.log( "Option ", i, ": ", usedLines[i], lines[ usedLines[i] ], c, lines[ usedLines[i][c] ] );
            
            var option = lines[ usedLines[i] ][c];
            if ( option == undefined )
            {
                option = lines[ usedLines[i] ][0];
            }
            $( "#answer" + (i+1) ).val( option );
        }

        $( ".letter-entry" ).show();
        if ( lines[l].length == 4 )
        {
            $( "#letter5" ).hide();
        }
        else if ( lines[l].length == 3 )
        {
            $( "#letter4" ).hide();
            $( "#letter5" ).hide();
        }
    }

    $( ".answer" ).click( function() {
        var guess = $( this ).val();
        var correct = lines[l][c];
        $( "#next-question" ).show();

        $( "#letter" + (c+1) ).val( guess );

        console.log( "Guess", guess, "Correct", correct );

        totalQuestions += 1;

        if ( guess == correct )
        {
            $( ".letter-entry" ).css( "color", "#188c13" );
            totalCorrect += 1;
            $( "#message" ).html( "Correct!" );
        }
        else
        {
            $( ".letter-entry" ).css( "color", "#c22d2d" );
            $( "#message" ).html( "WRONG!" );
            $( "#message" ).css( "color", "#c22d2d" );
        }
        
        UpdateStats();
    } );

    $( "#next-question" ).click( function() {
        NewQuestion();
    } );

    function UpdateStats()
    {
        $( "#total-correct" ).html( "Total correct: " + totalCorrect );
        $( "#total-questions" ).html( "Questions answered: " + totalQuestions );
    }

    NewQuestion();
    UpdateStats();
} );
