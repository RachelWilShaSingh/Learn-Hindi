# Shida-Template-Project

![Shida Logo](https://bytebucket.org/moosaderllc/shida/raw/712a4660e725132ef1f7a9424ceb40708250590d/shidalogo.png)

Template project you can begin working on a game with,
using the Shida engine (https://bitbucket.org/moosaderllc/shida/).

Template project contains common features like:
* Title state
* Help state
* Options state
* Game state
    * Movable character
* Multiple languages

More to come...
