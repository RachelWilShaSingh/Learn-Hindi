$( document ).ready( function()
{
    var words = [
        'अजगर',
        'कमल',
        'ताला',
        'हरा',
        'गिलास',
        'दिन',
        'टिकट',
        'नानी',
        'दादी',
        'पीला',
        'चुहिया',
        'सुबह',
        'चूहा',
        'सूरज',
        'फूल',
        'जलेबी',
        'केले',
        'पैसा',
        'भैया',
        'होली',
        'मोर',
        'कौशिक',
        'नौकर',
        'बंदर',
        'अंडा',
        'चाँद',
        'आँख',
        'अनार',
        'आम',
        'इमली',
        'ईख',
        'उल्लू',
        'ऊन',
        'ऋषि',
        'एड़ी',
        'ऐनक',
        'ओखली',
        'औरत',
        'अंगूर',
        'कमल',
        'खरगोश',
        'गणेश',
        'घड़ी',
        'चरखा',
        'जहाज',
        'झंडा',
        'टमाटर',
        'ठठेरा',
        'डमरू',
        'ढकना',
        'बाण',
        'तलवार',
        'थन',
        'दवात',
        'धनुष',
        'नगाड़ा',
        'भगत',
        'मछली',
        'यज्ञ',
        'रथ',
        'लड़की',
        'वजन',
        'शरबत',
        'षट्कोण',
        'सपेरा',
        'हल',
        'क्षत्रिय',
        'त्रिशूल',
        'छत्री',
        'ज्ञानी',
        'कलम',
        'चरखा'
            ];

    var scores = {};

    var totalCorrect = 0, totalQuestions = 0;
    var currentPhrase = "";

    function InitScores()
    {
        for ( var i = 0; i < words.length; i++ )
        {
            scores[ words[i] ] = {
                "word" : words[i],
                "correct" : 0,
                "incorrect" : 0
                };

            var tableRow = "<tr id='" + words[i] + "'>"
                + "<td>" + words[i] + "</td><td class='correct'>0</td><td class='incorrect'>0</td>"
                + "</tr>";

            $( "#word-stats" ).append( tableRow );
        }
    }

    function NewQuestion()
    {
        currentPhrase = words[ Math.floor( Math.random() * words.length ) ];
        
        $( "#next-question" ).hide();
        $( "#check-answer" ).show();
        $( "#message" ).html( "Spell the word given" );
        $( "#message" ).css( "color", "#053680" );
        $( "#answer" ).val( "" );

        setTimeout( PlaySound, 1000 );
    }

    function RemoveUsedQuestion()
    {
        var indexOf = words.indexOf( currentPhrase );
        words.splice( indexOf, 1 );
        console.log( words );
    }

    $( ".key" ).click( function() {
        var val = $( "#answer" ).val() + $( this ).val();
        $( "#answer" ).val( val );
    } );

    $( ".backspace" ).click( function() {
        var val = $( "#answer" ).val();
        val = val.substring( 0, val.length - 1 );
        $( "#answer" ).val( val );
    } );

    $( "#next-question" ).click( function() {
        NewQuestion();
    } );

    $( "#play-sound" ).click( function() {
        PlaySound();
    } );

    $( "#next-question" ).click( function() {
        NewQuestion();
    } );

    $( "#check-answer" ).click( function() {
        $( "#next-question" ).show();
        $( "#check-answer" ).hide();
        totalQuestions += 1;

        var guess = $( "#answer" ).val();

        if ( guess == currentPhrase )
        {
            totalCorrect += 1;
            $( "#message" ).html( "Correct!" );
            $( "#message" ).css( "color", "#199513" );
            scores[ currentPhrase ].correct += 1;
            $( "#" + currentPhrase ).find( ".correct" ).html( scores[ currentPhrase ].correct );
            RemoveUsedQuestion();  
        }
        else
        {
            $( "#message" ).html( "WRONG! Correct spelling: " + currentPhrase );
            $( "#message" ).css( "color", "#c22d2d" );
            $( "#incorrect-words" ).append( "<li>" + currentPhrase + "</li>" );
            scores[ currentPhrase ].incorrect += 1;
            $( "#" + currentPhrase ).find( ".incorrect" ).html( scores[ currentPhrase ].incorrect );
        }
      
        UpdateStats();
    } );

    function PlaySound()
    {
        console.log( currentPhrase );
        var audio = new Audio( "audio/" + currentPhrase + ".mp3" );
        audio.play();
    }

    function UpdateStats()
    {
        $( "#total-correct" ).html( "Correct: " + totalCorrect );
        $( "#total-questions" ).html( "Answered: " + totalQuestions );
        $( "#total-remaining" ).html( "Remaining: " + words.length );
    }

    $( "#toggle-words" ).click( function() {
        if ( $( ".word-list" ).css( "display" ) == "none" ) {
            $( ".word-list" ).css( "display", "block" );
            $( "#toggle-words" ).html( "⯅  Hide word list");
        }
        else {
            $( ".word-list" ).css( "display", "none" );
            $( "#toggle-words" ).html( "⯆  View word list");
        }
    } );

    NewQuestion();
    UpdateStats();
    InitScores();
} );
