$( document ).ready( function()
{
    var words = [
        // Animals
        { "dv" : "पक्षी",   "hindi" : "pakshee",    "english" : "bird" },
        { "dv" : "गाय",   "hindi" : "gaay",       "english" : "cow" },
        { "dv" : "मगर",   "hindi" : "magar",      "english" : "crocodile" },
        { "dv" : "हाथी",   "hindi" : "haathee",    "english" : "elephant" },
        { "dv" : "मछली",   "hindi" : "machhalee",  "english" : "fish" },
        { "dv" : "बकरा",   "hindi" : "bakara",     "english" : "goat" },
        { "dv" : "घोड़ा",   "hindi" : "ghoda",      "english" : "horse" },
        { "dv" : "बन्दर",   "hindi" : "bandar",     "english" : "monkey" },
        { "dv" : "उल्लू",   "hindi" : "ulloo",      "english" : "owl" },
        { "dv" : "तोता",   "hindi" : "tota",       "english" : "parrot" },
        { "dv" : "कबूतर",   "hindi" : "kabootar",   "english" : "pigeon" },
        { "dv" : "खरगोश",   "hindi" : "khargosh",   "english" : "rabbit" },
        { "dv" : "सांप",   "hindi" : "saanp",      "english" : "snake" },
        { "dv" : "ऊन",   "hindi" : "oon",        "english" : "wool" },

        // Dates
        { "dv" : "रविवार",   "hindi" : "ravivaar",       "english" : "sunday" },
        { "dv" : "सोमवार",   "hindi" : "somavaar",       "english" : "monday" },
        { "dv" : "मंगलवार",   "hindi" : "mangalvaar",    "english" : "tuesday" },
        { "dv" : "बुधवार",   "hindi" : "budhavaar",      "english" : "wednesday" },
        { "dv" : "गुरूवार",   "hindi" : "guroovaar",      "english" : "thursday" },
        { "dv" : "शुक्रवार",   "hindi" : "shukravaar",     "english" : "friday" },
        { "dv" : "शनिवार",   "hindi" : "shanivaar",      "english" : "saturday" },
        { "dv" : "दिन",   "hindi" : "din",              "english" : "day" },
        { "dv" : "सप्ताह",   "hindi" : "saptaah",         "english" : "week" },
        { "dv" : "महीना",   "hindi" : "maheena",         "english" : "month" },
        { "dv" : "साल",   "hindi" : "saal",             "english" : "year" },

        // Foods
        { "dv" : "गाजर",   "hindi" : "gaajar",             "english" : "carrot" },
        { "dv" : "फूलगोभी",   "hindi" : "phulgobhee",             "english" : "cauliflower" },
        { "dv" : "बैंगन",   "hindi" : "baingan",             "english" : "eggplant" },
        { "dv" : "मेथी",   "hindi" : "methee",             "english" : "fenugreek" },
        { "dv" : "फूल",   "hindi" : "phul",             "english" : "fruit" },
        { "dv" : "लहसुन",   "hindi" : "lahasun",             "english" : "garlic" },
        { "dv" : "अदरक",   "hindi" : "adarak",             "english" : "ginger" },
        { "dv" : "लौकी",   "hindi" : "laukee",             "english" : "gourd" },
        { "dv" : "अंगूर",   "hindi" : "angoor",             "english" : "grapes" },
        { "dv" : "शर्बत",   "hindi" : "sharbat",             "english" : "juice" },
        { "dv" : "आम",   "hindi" : "aam",             "english" : "mango" },
        { "dv" : "खरबूज",   "hindi" : "kharabooj",             "english" : "melon" },
        { "dv" : "भिन्डी",   "hindi" : "bhindee",             "english" : "okra" },
        { "dv" : "प्याज",   "hindi" : "pyaaj",             "english" : "onion" },
        { "dv" : "मटर",   "hindi" : "matar",             "english" : "peas" },
        { "dv" : "अनार",   "hindi" : "anaar",             "english" : "pomegranate" },
        { "dv" : "आलू",   "hindi" : "aaloo",             "english" : "potato" },
        { "dv" : "मूली",   "hindi" : "moolee",             "english" : "radish" },
        { "dv" : "इमली",   "hindi" : "imalee",             "english" : "tamarind" },
        { "dv" : "टमाटर",   "hindi" : "tamaatar",             "english" : "tomato" },
        { "dv" : "पानी",   "hindi" : "paanee",             "english" : "water" },
        { "dv" : "मिर्ची ",   "hindi" : "mirchee",             "english" : "hot/chili" },

        // Locations
        { "dv" : "बिल",   "hindi" : "bil",             "english" : "burrow" },
        { "dv" : "पिंजरा",   "hindi" : "pinjare",             "english" : "cage" },
        { "dv" : "देश",   "hindi" : "deshaa",             "english" : "country" },
        { "dv" : "खेत",   "hindi" : "khet",             "english" : "farm" },
        { "dv" : "बगीचे",   "hindi" : "bageecha",             "english" : "garden" },
        { "dv" : "घर",   "hindi" : "ghar",             "english" : "home" },
        { "dv" : "जंगल",   "hindi" : "jangal",             "english" : "jungle" },
        { "dv" : "घोंसला",   "hindi" : "ghonsale",             "english" : "nest" },
        { "dv" : "पाठशालय",   "hindi" : "paathashaalaa",             "english" : "school" },
        { "dv" : "दरबे",   "hindi" : "darabe",             "english" : "windowsill" },

        // Occupations
        { "dv" : "हलवाई",   "hindi" : "halwaaee",             "english" : "confectioner" },
        { "dv" : "ठठेरा",   "hindi" : "thathera",             "english" : "copper smith" },
        { "dv" : "डाक्टर",   "hindi" : "daaktar",             "english" : "doctor" },
        { "dv" : "किसान",   "hindi" : "kisaan",             "english" : "farmer" },
        { "dv" : "माली",   "hindi" : "malee",             "english" : "gardner" },
        { "dv" : "क्षत्रिय",   "hindi" : "kshatriya",             "english" : "kshatriya" },
        { "dv" : "डाकिया",   "hindi" : "daakiya",             "english" : "postman" },
        { "dv" : "सपेरा",   "hindi" : "sapera",             "english" : "snake charmer" },
        { "dv" : "सैनिक",   "hindi" : "sainek",             "english" : "soldier" },
        { "dv" : "दर्जी",   "hindi" : "darjee",             "english" : "tailor" },
        { "dv" : "शिक्षक",   "hindi" : "shikshak",             "english" : "teacher" },
        { "dv" : "धोबी",   "hindi" : "dhobee",             "english" : "washer" },

        // People
        { "dv" : "लड़की",   "hindi" : "ladkee",             "english" : "girl" },
        { "dv" : "आदमी",   "hindi" : "admi",             "english" : "man" },
        { "dv" : "रोगी",   "hindi" : "rogee",             "english" : "patient" },
        { "dv" : "छात्र",   "hindi" : "chhaatr",             "english" : "student" },
        { "dv" : "ज्ञानी",   "hindi" : "gyaanee",             "english" : "wise person" },
        { "dv" : "औरत",   "hindi" : "aurat",             "english" : "woman" },
        { "dv" : "भगत",   "hindi" : "bhagat",             "english" : "woshipper" },

        // Objects
        { "dv" : "घड़ी",   "hindi" : "ghadee",             "english" : "clock" },
        { "dv" : "ढकना",   "hindi" : "dhakana",             "english" : "cover/lid" },
        { "dv" : "झंडा",   "hindi" : "jhanda",             "english" : "flag" },
        { "dv" : "ऐनक",   "hindi" : "ainak",             "english" : "glasses/spectacles" },
        { "dv" : "दवात",   "hindi" : "dawaat",             "english" : "ink" },
        { "dv" : "पतंग",   "hindi" : "patang",             "english" : "kite" },
        { "dv" : "दवा",   "hindi" : "dava",             "english" : "medicine" },
        { "dv" : "कलम",   "hindi" : "kalam",             "english" : "pen" },
        { "dv" : "चरखा",   "hindi" : "charkha",             "english" : "spinning wheel" },
        { "dv" : "मिठाइ",   "hindi" : "methaaee",             "english" : "sweets" },
        { "dv" : "छतरी",   "hindi" : "chhataree",             "english" : "umbrella" },
        { "dv" : "वजन",   "hindi" : "vajan",             "english" : "weight" },

        // Weapons
        { "dv" : "बाण",   "hindi" : "baan",             "english" : "arrow" },
        { "dv" : "धनुष",   "hindi" : "dhanush",             "english" : "bow" },
        { "dv" : "तलवार",   "hindi" : "talvaar",             "english" : "sword" },
        { "dv" : "त्रिशूल",   "hindi" : "trishool",             "english" : "trident" },

        // Plants
        { "dv" : "कमल",   "hindi" : "kamal",             "english" : "lotus" },
        { "dv" : "ईख",   "hindi" : "eekh",             "english" : "reed" },
        { "dv" : "गुलाब",   "hindi" : "gulab",             "english" : "rose" },
        { "dv" : "सूर्यमुखी",   "hindi" : "sooryamukhee",             "english" : "sunflower" },
        { "dv" : "पेड़ ",   "hindi" : "ped",             "english" : "trees" },

        // Verbs
        { "dv" : "खेती ",   "hindi" : "khetee",             "english" : "farms" },
        { "dv" : "देना ",   "hindi" : "dena",             "english" : "gives" },
        { "dv" : "बनाकर ",   "hindi" : "banaakar",             "english" : "makes" },
        { "dv" : "सीता ",   "hindi" : "seetaa",             "english" : "sews" },
        { "dv" : "धोता ",   "hindi" : "dhotaa",             "english" : "washes" },

        // Misc
        { "dv" : "रथ ",   "hindi" : "rath",             "english" : "chariot" },
        { "dv" : "जहाज ",   "hindi" : "jahaaj",             "english" : "ship" },
        { "dv" : "षटकोण ",   "hindi" : "shatkon",             "english" : "hexagon" },
        { "dv" : "डमरु ",   "hindi" : "damru",             "english" : "drum" },
        { "dv" : "एड़ी  ",   "hindi" : "edee",             "english" : "heel" },
        { "dv" : "कपड़े ",   "hindi" : "kapade",             "english" : "clothes" },
        { "dv" : "गणेश ",   "hindi" : "ganesh",             "english" : "ganesh" },
    ];

    devanagari = null;
    hindi = null;
    english = null;

    totalQuestions = 0;
    totalCorrect = 0;

    function Random( min, max ) {
        return Math.floor( Math.random() * (max - min + 1) ) + min;
    }

    function RemoveFromList() {
        var removeIndex = -1;
        for ( var i = 0; i < words.length; i++ )
        {
            if ( words[i]["english"] == english ) {
                removeIndex = i;
                break;
            }
        }
        
        words.splice( removeIndex, 1 );
    }
    
    function NewQuestion() {
        var random = Random( 0, words.length - 1 );
        devanagari  = words[random]["dv"];
        hindi       = words[random]["hindi"];
        english     = words[random]["english"];

        $( "#what-is" ).html( english );
        $( "#hint-text" ).html( "&nbsp;" );
        $( "#remaining-words" ).html( words.length );
        $( "#user-word" ).val( "" );
        $( "#total-correct" ).html( totalCorrect );
        $( "#total-questions" ).html( totalQuestions );
        $( "#result" ).html( "&nbsp;" );
        $( "#result" ).removeClass( "correct" );
        $( "#result" ).removeClass( "incorrect" );
        $( "#user-submit" ).addClass( "btn-primary" );
        $( "#user-submit" ).removeClass( "btn-danger" );
        $( "#user-submit" ).removeClass( "btn-success" );
        $( "#user-submit" ).val( "Check answer" );
        $( "#image" ).attr( "src", "images/" + hindi + ".jpg" );

        setTimeout( PlayAudio, 500 );
    }

    function CheckAnswer() {
        var userAnswer = $( "#user-word" ).val();

        if ( userAnswer == devanagari ) {
            totalCorrect++;
            $( "#result" ).html( "Correct!" );
            $( "#result" ).addClass( "correct" );
            $( "#user-submit" ).addClass( "btn-success" );
            RemoveFromList();
        }
        else {
            $( "#result" ).html( "Wrong! The answer is " + devanagari );
            $( "#result" ).addClass( "incorrect" );
            $( "#user-submit" ).addClass( "btn-danger" );
        }

        $( "#user-submit" ).val( "Next question" );
        $( "#user-submit" ).removeClass( "btn-primary" );

        totalQuestions++;
    }

    function GiveHint() {
        $( "#hint-text" ).html( "Transliterated, it is " + hindi );
    }

    function PlayAudio() {
        var audio = new Audio( "audio/" + devanagari + ".mp3" );
        audio.play();
    }

    NewQuestion();

    $( "#user-hint" ).click( function() {
        GiveHint();
    } );

    $( "#user-listen" ).click( function() {
        PlayAudio();
    } );

    $( "#user-submit" ).click( function() {
        var text = $( "#user-submit" ).val();

        if ( text == "Check answer" )
            CheckAnswer();
        else
            NewQuestion();
    } );
} );
