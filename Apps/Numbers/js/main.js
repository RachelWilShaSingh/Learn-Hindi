$( document ).ready( function()
{
    var words = [
        [ "एक", "1" ],
        [ "दो", "2" ],
        [ "तीन", "3" ],
        [ "चार", "4" ],
        [ "पाँच", "5" ],
        [ "छः", "6" ],
        [ "सात", "7" ],
        [ "आठ", "8" ],
        [ "नौ", "9" ],
        [ "दस", "10" ],
    ];

    var numberHindi;
    var numberEnglish;
    var numberIndex;

    var totalCorrect = 0, totalQuestions = 0;
    function UpdateStats()
    {
        $( "#total-correct" ).html( "Total correct: " + totalCorrect );
        $( "#total-questions" ).html( "Questions answered: " + totalQuestions );
    }

    function IsCorrect()
    {
        $( ".letter-entry" ).css( "color", "#188c13" );
        totalCorrect += 1;
        $( "#message" ).html( "Correct!" );
        UpdateStats();
        PlaySound();
    }

    function IsWrong()
    {
        $( ".letter-entry" ).css( "color", "#c22d2d" );
        $( "#message" ).html( "WRONG!" );
        $( "#message" ).css( "color", "#c22d2d" );
        UpdateStats();
    }

    function PlaySound()
    {
        var audio = new Audio( "audio/" + numberHindi + ".mp3" );
        audio.play();
    }
    
    function NewQuestion()
    {
        $( "#next-question" ).hide();
        
        numberIndex = Math.floor( Math.random() * words.length );
        numberHindi = words[ numberIndex ][0];
        numberEnglish = words[ numberIndex ][1];
        console.log( numberHindi, numberEnglish );

        $( "#color" ).html( numberHindi );
        $( "#message" ).html( "What is this number?" );
    }

    $( ".answer" ).click( function() {
        var guess = $( this ).val();
        var correct = numberEnglish;

        totalQuestions += 1;

        if ( guess == correct )
        {
            IsCorrect();

            // Remove this word from the list
            words.splice( numberIndex, 1 );

            $( this ).css( "background", "#ffd200" );
        }
        else
        {
            IsWrong();
        }

        if ( words.length > 0 )
        {
            $( "#next-question" ).show();
        }
        else
        {
            $( "#message" ).html( "You got them all!" );
        }
        
        UpdateStats();
    } );

    $( "#next-question" ).click( function() {
        NewQuestion();
    } );

    NewQuestion();
    UpdateStats();
} );
