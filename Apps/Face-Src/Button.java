import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Button extends Actor
{
    String text;
    protected String soundFile;
    protected String nameEnglish;
    protected String nameHindi;
    protected String nameRomanized;
    
    public Button(String t, String newSound, String ne, String nh, String nr)
    {
        text = t;
        soundFile = newSound;
        nameEnglish = ne;
        nameHindi = nh;
        nameRomanized = nr;
    }
    
    public void act() 
    {
        getWorld().showText(text, getX(), getY());
        if ( Greenfoot.mouseClicked( this ) )
        {
            //Greenfoot.playSound( soundFile );
            int x = 500;
            int y = 30;
            getWorld().showText(nameEnglish, x, y);
            getWorld().showText(nameHindi, x, y+20);
            getWorld().showText(nameRomanized, x, y+40);
        }
    }    
}
