console.log( "title.js" );

titleState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,

    letters: [
        { dev: "क",     eng: "ka",    audio: "ka.wav" },
        { dev: "ख",     eng: "kha",    audio: "kha.wav" },
        { dev: "ग",     eng: "ga",    audio: "ga.wav" },
        { dev: "घ",     eng: "gha",    audio: "gha.wav" },
        { dev: "च",     eng: "cha",    audio: "cha.wav" },
        { dev: "छ",     eng: "chha",    audio: "chha.wav" },
        { dev: "ज",     eng: "ja",    audio: "ja.wav" },
        { dev: "झ",     eng: "jha",    audio: "jha.wav" },
        { dev: "ट",     eng: "ṭa",    audio: "ta1.wav" },
        { dev: "ठ",     eng: "ṭha",    audio: "tha1.wav" },
        { dev: "ड",     eng: "ḍa",    audio: "da1.wav" },
        { dev: "ढ",     eng: "ḍha",    audio: "dha1.wav" },
        { dev: "ण",     eng: "ṇa",    audio: "na1.wav" },
        { dev: "त",     eng: "ta",    audio: "ta2.wav" },
        { dev: "थ",     eng: "tha",    audio: "tha2.wav" },
        { dev: "द",     eng: "da",    audio: "da2.wav" },
        { dev: "ध",     eng: "dha",    audio: "dha2.wav" },
        { dev: "न",     eng: "na",    audio: "na2.wav" },
        { dev: "प",     eng: "pa",    audio: "pa.wav" },
        { dev: "फ",     eng: "pha",    audio: "pha.wav" },
        { dev: "ब",     eng: "ba",    audio: "ba.wav" },
        { dev: "भ",     eng: "bha",    audio: "bha.wav" },
        { dev: "म",     eng: "ma",    audio: "ma.wav" },
        { dev: "य",     eng: "ya",    audio: "ya.wav" },
        { dev: "र",     eng: "ra",    audio: "ra.wav" },
        { dev: "ल",     eng: "la",    audio: "la.wav" },
        { dev: "व",     eng: "va",    audio: "va.wav" },
        { dev: "श",     eng: "śa",    audio: "sha1.wav" },
        { dev: "ष",     eng: "ṣa",    audio: "sha2.wav" },
        { dev: "स",     eng: "sa",    audio: "sa.wav" },
        { dev: "ह",     eng: "ha",    audio: "ha.wav" },
        { dev: "क्ष",     eng: "ksha",  audio: "ksha.wav" },
        //{ dev: "त्र",     eng: "tra",   audio: "tra.wav" }, // doesn't render right
        { dev: "ज्ञ",     eng: "gya",   audio: "gya.wav" },
    ],

    questions: [],
    options: [],

    score: 0,
    totalQuestions: 0,

    Init: function( canvas, options ) {
        titleState.canvas = canvas;
        titleState.config = options;
        titleState.isDone = false;

        titleState.Reset();
    },

    Reset: function() {
        titleState.score = 0;
        titleState.totalQuestions = 0;
        titleState.questions = [];
        titleState.options = [];

        UI_TOOLS.ClearUI();
        
        // copy
        for ( var i = 0; i < titleState.letters.length; i++ )
        {
            titleState.questions.push( titleState.letters[i] );
        }

        // add sounds
        for ( var i = 0; i < titleState.letters.length; i++ )
        {
            ASSET_TOOLS.AddSound( { "title": titleState.letters[i]["audio"],
                volume: 1, loop: false, isMusic: false, file: "assets/audio/" + titleState.letters[i]["audio"]
                } );
        }
            ASSET_TOOLS.AddSound( { "title": "WRONG",
                volume: 1, loop: false, isMusic: false, file: "assets/audio/wrong.wav"
                } );

        UI_TOOLS.CreateImage( { title: "background",
                            src:    "assets/images/background.png",
                            x: 0, y: 0,
                            width: 640, height: 480,
                            fullWidth: 640, fullHeight: 480 } );
                            
        UI_TOOLS.CreateText( { title: "question", words: "Asdf",
                            color: "#000000", font: "bold 100px Sans-serif", x: 285, y: 175 } );


        var text = "Programmed by Rachel Singh, voice by Rai Singh";
        UI_TOOLS.CreateText( { title: "by", words: text,
                             color: "#000000", font: "bold 11px sans-serif",
                             x: 10,
                             y: titleState.config.height - 10 } );
                             
        UI_TOOLS.CreateText( { title: "what", words: "What is this?",
                             color: "#ffffff", font: "bold 30px sans-serif",
                             x: 200,
                             y: 300 } );

                             
        UI_TOOLS.CreateText( { title: "total-questions", words: "Questions: " + titleState.totalQuestions,
                             color: "#ffffff", font: "bold 20px sans-serif",
                             x: 10,
                             y: 50 } );
                             
        UI_TOOLS.CreateText( { title: "total-correct", words: "Correct: " + titleState.score,
                             color: "#ffffff", font: "bold 20px sans-serif",
                             x: 10,
                             y: 100 } );
                             
        UI_TOOLS.CreateText( { title: "total-remaining", words: "Remaining: " + titleState.questions.length,
                             color: "#ffffff", font: "bold 20px sans-serif",
                             x: 10,
                             y: 150 } );

        UI_TOOLS.CreateButton( { title: "button1", words: "C1",
                             color: "#ffffff", font: "bold 30px serif",
                             src: "assets/images/button.png",
                             x: 50, y: titleState.config.height - 150,
                             textX: 40, textY: 45,
                             width: 125, height: 75,
                             fullWidth: 125, fullHeight: 75,
                             Click: function(){ titleState.CheckAnswer( 0 ); }
                              } );

        UI_TOOLS.CreateButton( { title: "button2", words: "C2",
                             color: "#ffffff", font: "bold 30px serif",
                             src: "assets/images/button.png",
                             x: 250, y: titleState.config.height - 150,
                             textX: 40, textY: 45,
                             width: 125, height: 75,
                             fullWidth: 125, fullHeight: 75,
                             Click: function(){ titleState.CheckAnswer( 1 ); }
                              } );

        UI_TOOLS.CreateButton( { title: "button3", words: "C3",
                             color: "#ffffff", font: "bold 30px serif",
                             src: "assets/images/button.png",
                             x: 450, y: titleState.config.height - 150,
                             textX: 40, textY: 45,
                             width: 125, height: 75,
                             fullWidth: 125, fullHeight: 75,
                             Click: function(){ titleState.CheckAnswer( 2 ); }
                             } );
                             
        titleState.NextQuestion();
    },

    NextQuestion: function() {
        var letter = Math.floor( Math.random() * titleState.questions.length );

        titleState.question = titleState.questions[letter]["dev"];
        titleState.answer = titleState.questions[letter]["eng"];
        titleState.audio = titleState.questions[letter]["audio"];

        //ASSET_TOOLS.PlaySound( titleState.questions[0]["audio"] );

        UI_TOOLS.UpdateText( "question", titleState.question );

        var answers = [];
        
        // Copy
        for ( var i = 0; i < titleState.letters.length; i++ )
        {
            if ( titleState.letters[i]["eng"] != titleState.answer )
            {
                answers.push( titleState.letters[i] );
            }
        }
        
        answers.splice( letter, 1 );  // remove answer
        console.log( "Total answers", answers.length );
        
        for ( var i = 0; i < 3; i++ )
        {
            var rand = Math.floor( Math.random() * answers.length );
            titleState.options[i] = answers[rand]["eng"];
            
            answers.splice( rand, 1 );
            var name = "button" + (i+1);
            UI_TOOLS.UpdateText( name, titleState.options[i] );
        }

        // Replace one with the right answer
        var randAnswer = Math.floor( Math.random() * 3 );
        titleState.options[randAnswer] = titleState.answer;
        var name = "button" + (randAnswer+1);
        UI_TOOLS.UpdateText( name, titleState.answer );

        // Remove the letter selected
        titleState.questions.splice( letter, 1 );
    },

    CheckAnswer: function( clicked ) {
        if ( titleState.options[clicked] == titleState.answer )
        {
            titleState.score += 1;
            var text = "CORRECT! " + titleState.question + " = " + titleState.answer;
            UI_TOOLS.UpdateText( "what", text );

            console.log( "Audio:", titleState.audio );
            ASSET_TOOLS.PlaySound( titleState.audio );
            
        }
        else
        {
            var text = "WRONG! " + titleState.question + " = " + titleState.answer;
            UI_TOOLS.UpdateText( "what", text );
            ASSET_TOOLS.PlaySound( "WRONG" );
        }

        titleState.totalQuestions += 1;

        UI_TOOLS.UpdateText( "total-questions", "Questions: " + titleState.totalQuestions );
        UI_TOOLS.UpdateText( "total-correct", "Correct: " + titleState.score );
        UI_TOOLS.UpdateText( "total-remaining", "Remaining: " + titleState.questions.length );

        if ( titleState.questions.length == 0 )
        //if ( 0 == 0 )
        {
            titleState.SetupGameOver();
        }
        else
        {
            titleState.NextQuestion();
        }
    },

    SetupGameOver: function() {
        UI_TOOLS.ClearUI();

        UI_TOOLS.CreateImage( { title: "background",
                            src:    "assets/images/background2.png",
                            x: 0, y: 0,
                            width: 640, height: 480,
                            fullWidth: 640, fullHeight: 480 } );

        var y = 150;
                            
        UI_TOOLS.CreateText( { title: "total-questions", words: "Total Questions: " + titleState.totalQuestions,
                             color: "#ffffff", font: "bold 40px sans-serif",
                             x: 100,
                             y: y } );
                             
        UI_TOOLS.CreateText( { title: "total-correct", words: "Correct: " + titleState.score,
                             color: "#ffffff", font: "bold 40px sans-serif",
                             x: 200,
                             y: y+50 } );

        var ratio = Math.floor( titleState.score / titleState.totalQuestions * 100 );
        var grade = "F";
        if ( ratio >= 90 ) { grade = "A"; }
        else if ( ratio >= 80 ) { grade = "B"; }
        else if ( ratio >= 70 ) { grade = "C"; }
        else if ( ratio >= 60 ) { grade = "D"; }
                             
        UI_TOOLS.CreateText( { title: "ratio", words: "Ratio: " + ratio + "%",
                             color: "#ffffff", font: "bold 40px sans-serif",
                             x: 200,
                             y: y+100 } );

                             
        UI_TOOLS.CreateText( { title: "grade", words: "Grade: " + grade,
                             color: "#ffffff", font: "bold 40px sans-serif",
                             x: 200,
                             y: y+150 } );


        UI_TOOLS.CreateButton( { title: "reset", words: "Reset",
                             color: "#ffffff", font: "bold 20px sans-serif",
                             src: "assets/images/button.png",
                             x: 250, y: 400,
                             textX: 30, textY: 45,
                             width: 125, height: 75,
                             fullWidth: 125, fullHeight: 75,
                             Click: function(){ titleState.Reset(); }
                             } );
    },

    Clear: function() {
        UI_TOOLS.ClearUI();
    },

    Click: function( ev ) {
        UI_TOOLS.Click( ev );
    },

    KeyPress: function( ev ) {
    },

    KeyRelease: function( ev ) {
    },

    Update: function() {
    },

    Draw: function() {
        UI_TOOLS.Draw( titleState.canvas );
    },
};
