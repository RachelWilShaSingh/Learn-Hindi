import os
from os import listdir
from os.path import isfile, join

onlyfiles = [f for f in listdir( "." ) if isfile(join( "." , f))]


for f in onlyfiles:
    if ".wav" in f:
        print( f )
        os.system( "ffmpeg -i " + f + " " + f + ".mp3" )
