#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

words = [
    { "english" : "grapes", "anglicized" : "angoor", "hindi" : "अंगूर" },
    { "english" : "अनार", "anglicized" : "anaar", "hindi" : "अनार" },
    { "english" : "mango", "anglicized" : "aam", "hindi" : "आम" },
    { "english" : "tamarind", "anglicized" : "imalee", "hindi" : "इमली" },
    { "english" : "reed", "anglicized" : "eekh", "hindi" : "ईख" },
    { "english" : "owl", "anglicized" : "ulloo", "hindi" : "उल्लू" },
    { "english" : "wool", "anglicized" : "oon", "hindi" : "ऊन" },
    { "english" : "sage", "anglicized" : "rishi", "hindi" : "ऋषि" },
    { "english" : "heel", "anglicized" : "edee", "hindi" : "एड़ी" },
    { "english" : "specs", "anglicized" : "ainak", "hindi" : "ऐनक" },
    { "english" : "hollow", "anglicized" : "okhalee", "hindi" : "hollow" },
    { "english" : "woman", "anglicized" : "aurat", "hindi" : "औरत" },
    { "english" : "kamal", "anglicized" : "lotus", "hindi" : "कमल" },
    { "english" : "pen", "anglicized" : "kalam", "hindi" : "कलम" },
    { "english" : "kshatriya", "anglicized" : "kshatriya", "hindi" : "क्षत्रिय" },
    { "english" : "rabbit", "anglicized" : "kharagosh", "hindi" : "खरगोश" },
    { "english" : "ganesh", "anglicized" : "ganesh", "hindi" : "गणेश" },
    { "english" : "clock", "anglicized" : "ghadee", "hindi" : "घड़ी" },
    { "english" : "spinning wheel", "anglicized" : "charakha", "hindi" : "चरखा" },
    { "english" : "umbrella", "anglicized" : "chhatree", "hindi" : "छत्री" },
    { "english" : "ship", "anglicized" : "jahaaj", "hindi" : "जहाज" },
    { "english" : "wise person", "anglicized" : "gyaanee", "hindi" : "ज्ञानी" },
    { "english" : "flag", "anglicized" : "jhanda", "hindi" : "झंडा" },
    { "english" : "tomato", "anglicized" : "tamaatar", "hindi" : "टमाटर" },
    { "english" : "stomach", "anglicized" : "thathera", "hindi" : "ठठेरा" },
    { "english" : "drum", "anglicized" : "damru", "hindi" : "डमरू" },
    { "english" : "cover", "anglicized" : "dhakana", "hindi" : "ढकना" },
    { "english" : "sword", "anglicized" : "talvaar", "hindi" : "तलवार" },
    { "english" : "trident", "anglicized" : "trishool", "hindi" : "त्रिशूल" },
    { "english" : "?", "anglicized" : "than", "hindi" : "थन" },
    { "english" : "?", "anglicized" : "davaat", "hindi" : "दवात" },
    { "english" : "bow", "anglicized" : "dhanush", "hindi" : "धनुष" },
    { "english" : "?", "anglicized" : "nagaada", "hindi" : "नगाड़ा" },
    { "english" : "arrow", "anglicized" : "baan", "hindi" : "बाण" },
    { "english" : "worshipper", "anglicized" : "bhagat", "hindi" : "भगत" },
    { "english" : "fish", "anglicized" : "machhalee", "hindi" : "मछली" },
    { "english" : "pyre", "anglicized" : "yagya", "hindi" : "यज्ञ" },
    { "english" : "chariot", "anglicized" : "rath", "hindi" : "रथ" },
    { "english" : "girl", "anglicized" : "ladkee", "hindi" : "लड़की" },
    { "english" : "weight", "anglicized" : "vajan", "hindi" : "वजन" },
    { "english" : "syrup/juice", "anglicized" : "sharbat", "hindi" : "शरबत" },
    { "english" : "hexagon", "anglicized" : "shatkon", "hindi" : "षट्कोण" },
    { "english" : "snake charmer", "anglicized" : "sapera", "hindi" : "सपेरा" },
    { "english" : "solution", "anglicized" : "hal", "hindi" : "हल" },
    { "english" : "glasses", "anglicized" : "ainak", "hindi" : "ऐनक" },
    { "english" : "goat", "anglicized" : "bakree", "hindi" : "बकरी" },
    { "english" : "potato", "anglicized" : "aaloo", "hindi" : "आलू" },
    { "english" : "eggplant", "anglicized" : "baingan", "hindi" : "बैंगन" },
    { "english" : "onion", "anglicized" : "pyaaj", "hindi" : "प्याज" },
    { "english" : "cahuliflower", "anglicized" : "phoolagobhee", "hindi" : "फूलगोभी" },
    { "english" : "ginger", "anglicized" : "adarak", "hindi" : "अदरक" },
    { "english" : "chili", "anglicized" : "mirch", "hindi" : "मिर्च" },
    { "english" : "carrot", "anglicized" : "gaajar", "hindi" : "गाजर" },
    { "english" : "peas", "anglicized" : "matar", "hindi" : "मटर" },
    { "english" : "gourd", "anglicized" : "laukee", "hindi" : "लौकी" },
    { "english" : "fenugreek", "anglicized" : "methee", "hindi" : "मेथी" },
    { "english" : "radish", "anglicized" : "moolee", "hindi" : "मूली" },
    { "english" : "house", "anglicized" : "ghar", "hindi" : "घर" },
    { "english" : "elephant", "anglicized" : "haathee", "hindi" : "हाथी" },
    { "english" : "water", "anglicized" : "paanee", "hindi" : "पानी" },
    { "english" : "crocodile", "anglicized" : "magar", "hindi" : "मगर" },
    { "english" : "monkey", "anglicized" : "bandar", "hindi" : "बन्दर" },
    { "english" : "trees", "anglicized" : "ped", "hindi" : "पेड़" },
    { "english" : "pigeon", "anglicized" : "kabootar", "hindi" : "कबूतर" },
    { "english" : "bird", "anglicized" : "pakshee", "hindi" : "पक्षी" },
    { "english" : "parrot", "anglicized" : "totaa", "hindi" : "तोता" },
]

cards = []

wrong = {
}

while ( len( words ) > 0 ):
    r = random.randint( 0, len( words ) - 1 )
    cards.append( words[r] )
    del words[r]


done = False
i = 0

while ( len( cards ) > 0 and done is False ):
    
    print( "\t" + cards[i]["english"] + "\t" + cards[i]["anglicized"] )
    print( "" )
    print( "1. Show answer     2. Quit" )

    choice = input( ">> " );

    if ( choice == 2 ):
        done = True
        break

    print( "" )
    print( "\t" + cards[i]["hindi"] )
    print( "" )
    print( "1. Right \t 2. Wrong" )

    choice = input( ">> " )

    if ( choice == 2 ):
        if ( words[i]["hindi"] in wrong ):
            wrong[ cards[i]["hindi"] ] += 1
        else:
            wrong[ cards[i]["hindi"] ] = 1
    elif ( choice == 1 ):
        del cards[i]

    print( "\n\n" )

print( "\n\nWrong answers:" )

for w in wrong:
    print( w )
